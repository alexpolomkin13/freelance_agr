<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->text('text');
            $table->string('price');
            $table->integer('views')->default(0);
            $table->string('date');
            $table->string('detail_link');
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('freelance_id')->unsigned()->nullable();
            //$table->foreign('category_id')->references('id')->on('categories');
            //$table->foreign('freelance_id')->references('id')->on('freelances');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_work_exclude');
        Schema::dropIfExists('user_work_favorite');
        Schema::dropIfExists('work');
    }
}
