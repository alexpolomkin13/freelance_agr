<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
//Route::post('/', 'HomeController@index')->name('home');
Route::get('/test', 'Controller@test');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/register', 'AuthController@registerForm');
    Route::post('/register', 'AuthController@register');
    Route::get('/login', 'AuthController@loginForm');
    Route::post('/login', 'AuthController@login');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'AuthController@logout');
    Route::get('/profile', 'ProfileController@index');
    Route::post('/profile', 'ProfileController@update');
    Route::get('/works/excludes', 'ProfileController@showExcludesWorks')->name("excludeWorks");
    Route::get('/works/favorite', 'ProfileController@showFavoriteWorks')->name("favoriteWorks");
    Route::get('/works/notes', 'ProfileController@showNotes')->name("showNotes");

});
Route::post('work/create_note/{id}', 'NoteController@create')->name('createNote');
Route::post('/exclude/{id}', 'HomeController@exclude_work')->name('exclude');
Route::post('/favorite/{id}', 'HomeController@favorite_work')->name('favorite');


