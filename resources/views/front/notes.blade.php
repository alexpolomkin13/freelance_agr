@extends('front.layout')

@section('content')
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @if (session('status'))
                        <div class="alert alert-success">{{session('status')}}</div>
                    @endif
                        <div class="text-center">
                            <h3 class="text-uppercase">Сделанные заметки</h3>
                        </div>
                        <br>
                    @foreach($works as $work)
                        <article class="post">

                            <div class="post-content">
                                <header class="entry-header text-center text-uppercase">
                                    <h6><a href="#">{{$work->getCategoryTitle()}}</a></h6>

                                    <h1 class="entry-title"><a href="blog.html">{{$work->title}}</a></h1>


                                </header>
                                <div class="entry-content">
                                    <p>{{$work->text}}</p>

                                    <div class="btn-continue-reading text-center text-uppercase">
                                    <a href="{{$work->detail_link}}" class="more-link" target="_blank">Перейти на сайт</a>
                                        <a class="more-link note" onclick="$('.form_note_{{$work->id}}').show();">Добавить заметку</a>
                                    </div>

                                </div>

                                <div class="social-share">
                                    <span class="social-share-title pull-left text-capitalize">На сайт добавлено {{$work->created_at->diffForHumans()}}</span>
                                    <ul class="text-center pull-right">

                                        <span class="social-share-title pull-left">{{$work->getPrice()}}</span>
                                    </ul>
                                </div>
                                <div class="form_note_{{$work->id}}" hidden><!--leave comment-->
                                    <hr>
                                    <h4>Оставьте заметку</h4>


                                    {!! Form::open(['route' => ['createNote', $work->id], 'method' => 'post', 'class' => 'form-horizontal contact-form']) !!}
                                    <div class="form-group">
                                        <div class="col-md-12">
										<textarea class="form-control" name="text"
                                                  placeholder="Write Massage"></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-dark">Добавить</button>

                                    {!! Form::close() !!}
                                </div><!--end leave comment-->

                                    <div class="notes"><!--bottom comment-->
                                        <hr>
                                        <h4>Замтеки</h4>
                                        @foreach($work->notes as $note)
                                            <div class="comment-text">

                                                <p class="comment-date">
                                                    {{$note->created_at->format('M, d, Y - G:i')}}
                                                </p>


                                                <p class="para">{{$note->text}}</p>
                                            </div>
                                        @endforeach
                                    </div>
                            </div>
                        </article>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection
