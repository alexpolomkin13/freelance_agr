@extends('front.layout')

@section('content')
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @if (session('status'))
                        <div class="alert alert-success">{{session('status')}}</div>
                    @endif
                        <div class="text-center">
                            <h3 class="text-uppercase">Удалённые предожения</h3>
                        </div>
                        <br>
                    @foreach($works as $work)
                        <article class="post">

                            <div class="post-content">
                                <header class="entry-header text-center text-uppercase">
                                    <h6><a href="#">{{$work->getCategoryTitle()}}</a></h6>

                                    <h1 class="entry-title"><a href="blog.html">{{$work->title}}</a></h1>


                                </header>
                                <div class="entry-content">
                                    <p>{{$work->text}}</p>

                                    <div class="btn-continue-reading text-center text-uppercase">
                                    <a href="{{$work->detail_link}}" class="more-link" target="_blank">Перейти на сайт</a>
                                    </div>

                                </div>

                                <div class="social-share">
                                    <span class="social-share-title pull-left text-capitalize">На сайт добавлено {{$work->created_at->diffForHumans()}}</span>
                                    <ul class="text-center pull-right">

                                        <span class="social-share-title pull-left">{{$work->getPrice()}}</span>
                                    </ul>
                                </div>
                            </div>
                        </article>
                    @endforeach
                    {{$works->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
