<div class="col-md-4" data-sticky_column>
    <div class="primary-sidebar">

        {{--<aside class="widget news-letter">
            <h3 class="widget-title text-uppercase text-center">Get Newsletter</h3>

            <form action="#">
                <input type="email" placeholder="Your email address">
                <input type="submit" value="Subscribe Now"
                       class="text-uppercase text-center btn btn-subscribe">
            </form>

        </aside>--}}
        <aside class="widget pos-padding">
            <h3 class="widget-title text-uppercase text-center">Filter</h3>

            <div class="thumb-latest-posts">


                <div class="media">
                    @if ($errors->any())
                        @include('front.errors')
                    @endif
                    {!! Form::open(['route' => "home", 'method' => 'get', 'id' => 'filter']) !!}
                    <div class="form-group">
                                <label>Категории</label>
                                {{Form::select('categories[]',
                                $categories->pluck('title', 'id')->toArray(),
                                request()->categories,
                                ['class' => 'form-control select2',
                                    'multiple' => 'multiple',
                                    'data-placeholder' => 'Выберите категорию/и',
                                    ])}}
                            </div>
                            <!-- Date -->

                            <!-- checkbox -->
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" class="minimal" name="price_exist" value="1" {{ request()->price_exist == 1 || isset(request()->pricefrom) ? 'checked' : ''}}>
                                </label>
                                <label>
                                    Предложения с ценой
                                </label>
                            </div>
                    {{--<div class="form-group">
                        <input type="text" class="form-control" id="pricefrom" placeholder="цена от" name="pricefrom" value="{{request()->pricefrom}}">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="proceto" placeholder="цена до" name="priceto" value="{{request()->priceto}}">
                    </div>--}}
                    <div class="form-group">
                        <input type="text" class="form-control" id="keywords" placeholder="ключевые слова" name="keywords" value="{{request()->keywords}}">
                        <br>
                        <input type="text" class="form-control" id="minuswords" placeholder="минус слова" name="minuswords" value="{{request()->minuswords}}">
                    </div>


                    <div class="form-group">
                        <label >Цена</label>
                        <input type="text   " class="form-control" id="pricefrom" placeholder="цена от" name="pricefrom" value="{{request()->pricefrom}}">
                        <br>
                        <input type="text" class="form-control" id="priceto" placeholder="цена до" name="priceto" value="{{request()->priceto}}">

                    </div>

                    <button type="submit"  class="btn">Применить</button>

                    {!! Form::close() !!}
                </div>

        </aside>
        <aside class="widget border pos-padding">
            <h3 class="widget-title text-uppercase text-center">Categories</h3>
            <ul>
                @foreach($categories as $category)
                    <li>
                        <a href="#">{{$category->title}}</a>
                        <span class="post-count pull-right"> ({{$category->works()->count()}})</span>
                    </li>
                @endforeach
            </ul>
        </aside>
    </div>
</div>
