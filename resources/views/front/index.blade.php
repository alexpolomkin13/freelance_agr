@extends('front.layout')

@section('content')
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @if (session('status'))
                        <div class="alert alert-success">{{session('status')}}</div>
                    @endif
                        @if (session('error'))
                            <div class="alert alert-warning">{!!session('error')!!}</div>
                        @endif
                        {{--<div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Выводить по
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="/?pag=5">5</a>
                                <a class="dropdown-item" href="/?pag=10">10</a>
                                <a class="dropdown-item" href="/?pag=15">15</a>
                            </div>
                        </div>--}}
                    @foreach($works as $work)
                        <article class="post">

                            <div class="post-content">
                                <header class="entry-header text-center text-uppercase">
                                    <h6><a href="#">{{$work->getCategoryTitle()}}</a></h6>

                                    <h1 class="entry-title"><a href="{{$work->detail_link}}">{{$work->title}}</a></h1>


                                </header>
                                <div class="entry-content">
                                    <p>{{$work->text}}</p>

                                    <div class="btn-continue-reading text-center text-uppercase">
                                            {!! Form::open(['route' => ['exclude', $work->id], 'method' => 'post', 'class' => 'pull-left']) !!}
                                              <button   type="submit"  class="more-link exclude">Удалить</button>
                                            {!! Form::close() !!}

                                        <a href="{{$work->detail_link}}" class="more-link" target="_blank">Перейти на сайт</a>
                                        <a class="more-link note" onclick="$('.form_note_{{$work->id}}').show();">Добавить заметку</a>
                                                {!! Form::open(['route' => ['favorite', $work->id], 'method' => 'post', 'class' => 'pull-right']) !!}
                                                   <button type="submit" class="more-link favorite">В избранное</button>
                                                {!! Form::close() !!}

                                    </div>

                                </div>
                                <div class="social-share">
                                    <span class="social-share-title pull-left text-capitalize">На сайт добавлено {{$work->created_at->diffForHumans()}}</span>
                                    <ul class="text-center pull-right">

                                        <span class="social-share-title pull-left">{{$work->getPrice()}}</span>
                                    </ul>
                                </div>

                               {{-- <div class="text-center">
                                    <a href="{{$work->detail_link}}" class="more-link" target="_blank">Перейти на сайт</a>
                                </div>--}}

                                <div class="form_note_{{$work->id}}" hidden><!--leave comment-->
                                    <hr>
                                    <h4>Оставьте заметку</h4>


                                        {!! Form::open(['route' => ['createNote', $work->id], 'method' => 'post', 'class' => 'form-horizontal contact-form']) !!}
                                        <div class="form-group">
                                            <div class="col-md-12">
										<textarea class="form-control" name="text"
                                                  placeholder="Write Massage"></textarea>
                                            </div>
                                        </div>
                                    <button type="submit" class="btn btn-dark">Добавить</button>

                                    {!! Form::close() !!}
                                </div><!--end leave comment-->
                                @if(!empty($notes) && !$notes->where('work_id', $work->id)->isEmpty())
                                <div class="notes"><!--bottom comment-->
                                    <hr>
                                    <h4>Замтеки</h4>
                                    @foreach($notes->where('work_id', $work->id) as $note)
                                    <div class="comment-text">

                                        <p class="comment-date">
                                            {{$note->created_at->format('M, d, Y - G:i')}}
                                        </p>


                                        <p class="para">{{$note->text}}</p>
                                    </div>
                                        @endforeach
                                </div>
                                    @endif
                            </div>
                        </article>
                        @endforeach
                    {{$works->links()}}
                </div>
                @include('front._sidebar')
            </div>
        </div>
    </div>



    <script>
        $('div.alert-success').delay(1000).slideUp();
        $('div.alert-warning').delay(2000).slideUp();


    </script>
@endsection
