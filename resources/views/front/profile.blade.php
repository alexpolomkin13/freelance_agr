@extends('front.layout')

@section('content')
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">

                    <div class="leave-comment mr0"><!--leave comment-->

                        <h3 class="text-uppercase">My profile</h3>
                        @if($errors->any())
                            @include('admin.errors')
                        @endisset
                        @if (session('status'))
                            <div class="alert alert-success">{{session('status')}}</div>
                        @endif

                        <br>
                        <form class="form-horizontal contact-form" role="form" method="post" action="/profile" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="name" name="name"
                                           placeholder="Name" value="{{$user->name}}">
                                </div>
                            </div>
                           {{-- <div class="form-group">
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="email" name="email"
                                           placeholder="Email" value="{{$user->email}}" required>
                                </div>
                            </div>--}}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="password" class="form-control" id="password" name="password"
                                           placeholder="password">
                                </div>
                            </div>


                            <button type="submit" name="submit" class="btn send-btn">Update</button>

                        </form>
                    </div><!--end leave comment-->
                </div>
                <div class="col-md-4" data-sticky_column>
                    <div class="primary-sidebar">

                        <aside class="widget border pos-padding">
                            <ul>
                                <li>
                                    <a href="{{route('favoriteWorks')}}">Избранные предложения</a>
                                </li>
                                <li>
                                    <a href="{{route('excludeWorks')}}">Удалённые предложения</a>
                                </li>
                                <li>
                                    <a href="{{route('showNotes')}}">Сделанные заметки</a>
                                </li>

                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
