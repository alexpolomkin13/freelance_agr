<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    public static function add($fields)
    {
        $note = new static;
        $note->text = $fields['text'];
        $note->user_id = $fields["user_id"];
        $note->work_id = $fields["work_id"];

        $note->save();

        return $note;
    }

    public function remove()
    {
        $this->delete();
    }

    public function work()
    {
        return $this->belongsTo(Work::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
