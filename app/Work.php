<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{

    protected $fillable = ['title', 'text', 'price', 'date', 'detail_link'];

    public static function add($fields)
    {
        $work = new static;
        $work->fill($fields);
        $work->category_id = $fields["category_id"];
        $work->freelance_id = $fields["freelance_id"];
        $work->date = date("m.d.y");
        $work->views = 0;
        $work->save();

        return $work;
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }


    public function remove()
    {
        $this->delete();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function freelance()
    {
        return $this->belongsTo(Freelance::class);
    }

    public function users_f()
    {
        return $this->belongsToMany(
            User::class,
            'user_work_favorite',
            'work_id',
            'user_id'
        );
    }

    public function users_e()
    {
        return $this->belongsToMany(
            User::class,
            'user_work_exclude',
            'work_id',
            'user_id'
        );
    }

    public function getCategoryTitle()
    {
        return (!empty($this->category)) ? $this->category->title : 'Нет категории';
    }

    public function getPrice()
    {
        return (!empty($this->price)) ? $this->price : "Цена не указана";
    }

}
