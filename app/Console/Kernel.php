<?php

namespace App\Console;

use App\Category;
use App\Freelance;
use App\Helper;
use App\Logger;
use App\MoneyConvertor;
use App\Work;
use App\Parser;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Symfony\Component\DomCrawler\Crawler;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('command:daily-reset')->everyFiveMinutes();
        //парсинг биржы weblancer
        $schedule->call(function () {
            Logger::$PATH = storage_path('logs');
            $logger = Logger::getLogger('freelance');
            $settingsParser = [
                "settings" => [
                    "title" => 'div.title > a',
                    "text" => 'p.text_field',
                    "price" => 'div.title.amount',
                    "detail_link" => 'div.title > a',
                    "category" => 'div.text-muted.dot_divided > span > a',
                    "block" => 'div.cols_table > div.row.click_container-link.set_href'
                ]
            ];
            $idFreelance = Freelance::checkIsset('weblancer');
            if(empty($idFreelance)) {
                $idFreelance = Freelance::add(['name' => 'weblancer', 'url' => 'https://www.weblancer.net/']);
            }
            $categories = Category::all()->pluck('title', 'id')->toArray();
            $works = Work::where('freelance_id', $idFreelance)->pluck('title', 'id')->toArray();
            //инициализация класса конвертора валют

            $converter = new MoneyConvertor();


            $parser = new Parser();
            $content = [];
            for ($i = 1; $i < 2; $i++ )
            {
                $link = "https://www.weblancer.net/jobs/?type=project&page=$i";
                $html = file_get_contents($link);
                $content = $parser->getContent($settingsParser, $link, $html);

                foreach ($content as $item) {
                    if (!in_array($item['title'], $works)) {

                        if (Helper::checkIssetNumbers($item['price']) && Helper::isDollar($item['price'])) {
                            $item['price'] = $converter->Ruble2DollarConvertor($item['price']) . 'р.';
                        }

                        if (!in_array($item["category"], $categories)) {
                            $categories[] = $item["category"];
                            $item["category_id"] = Category::add($item["category"]);
                        } else {
                            $item["category_id"] = array_search($item["category"], $categories);
                        }
                        $item["freelance_id"] = $idFreelance;
                        Work::add($item);
                        $logger->log("Работа с url: " . $item['detail_url'] . ' успешно добавлена');
                    }
                }

            }
        })->daily();

        //парсинг биржы freelance
        $schedule->call(function () {

            $idFreelance = Freelance::checkIsset('freelance');
            if(empty($idFreelance)) {
                $idFreelance = Freelance::add(['name' => 'freelance', 'url' => 'https://freelance.ru/']);
            }
            $categories = Category::all()->pluck('title', 'id')->toArray();
            $works = Work::where('freelance_id', $idFreelance)->pluck('title', 'id')->toArray();

            $link = "https://freelance.ru/projects/";
            $html = file_get_contents($link);
            $html = mb_convert_encoding($html, "UTF-8", "windows-1251");
            $crawler = new Crawler($html, $link);

            $crawler = $crawler->filter("ul#nav-spec-list")->first()->filter('li')->slice(2);
            $flCategory = $crawler->each(function (Crawler $node, $i) {
                $data["category"] = trim(stristr($node->filter("a")->text(), '-', true));
                $data["ref"] = $node->filter("a")->link()->getUri();
                return $data;
            });

            $settingsParser = [
                "settings" => [
                    "title" => 'div.p_title > h2 > a > span',
                    "text" => 'a.descr > p > span:nth-child(2)',
                    "price" => 'a.descr > p > span',
                    "detail_link" => 'a.descr',
                    "block" => 'div.projects > div.proj'
                ]
            ];

            $parser = new Parser();
            $content = [];

            foreach ($flCategory as $category) {
                $html = file_get_contents($category["ref"]);
                $html = mb_convert_encoding($html, "UTF-8", "windows-1251");

                $content[$category['category']] = $parser->getContent($settingsParser, $category["ref"], $html);
            }
            foreach ($content as $category => $item) {
                foreach ($item as $data) {

                    if (!in_array($data['title'], $works)) {
                        if (!Helper::checkIssetNumbers($data['price'])) {
                            $data['price'] = '';
                        }

                        if (!in_array($category, $categories)) {
                            $categories[] = $category;
                            $data["category_id"] = Category::add($category);
                        } else {
                            $data["category_id"] = array_search($category, $categories);
                        }
                        $data["freelance_id"] = $idFreelance;
                        Work::add($data);
                    }
                }
            }
        })->daily();

        //check url exists

        $schedule->call(function () {
            Logger::$PATH = storage_path('logs');
            $logger = Logger::getLogger('broken_url');
            $works = Work::all()->pluck('detail_link', 'id')->toArray();
            foreach ($works as $id => $url) {
                $headers = @get_headers($url);
                if(!$headers || strpos( $headers[0], '404')) {
                    $logger->log("url: " . $url . ' не существует');
                }

            }
        })->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
