<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    use Sluggable;

    protected $fillable = ['title'];

    public function works()
    {
        return $this->hasMany(Work::class);
    }

    public static function add($name)
    {
        $category = new static();
        $category->title = $name;
        $category->save();

        return $category->id;
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
