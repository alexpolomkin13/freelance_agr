<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoneyConvertor extends Model
{
    private $courses2Ruble;

    public function __construct()
    {
        $courses = simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp");
        $data = [];
        foreach ($courses->Valute as $lang) {
            $code = $lang->CharCode->__toString();
            $data[$code] = $lang->Value->__toString();
        }
        $this->courses2Ruble = $data;
    }

    public  function Ruble2DollarConvertor($dol)
    {
        $dol = Helper::getNumberPart($dol);
        $usd = (int)$this->courses2Ruble['USD'];
        return round( $dol * $usd);

    }
}
