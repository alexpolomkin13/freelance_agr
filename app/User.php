<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function add($fields)
    {
        $user = new static;
        $user->fill($fields);
        $user->password = bcrypt($fields['password']);
        $user->save();

        return $user;
    }

    public function edit($fields)
    {
        if (empty($fields['password'])) {
            $fields['password'] = $this->password;
        } else {
            $fields['password'] = bcrypt($fields['password']);
        }
        $this->fill($fields);
        $this->save();
    }

    public function remove()
    {
        $this->delete();
    }

    public function favorite_works()
    {
        return $this->belongsToMany(
            Work::class,
            'user_work_favorite',
            'user_id',
            'work_id'
        );
    }

    public function exclude_works()
    {
        return $this->belongsToMany(
            Work::class,
            'user_work_exclude',
            'user_id',
            'work_id'
        );
    }


    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function excludeWork($ids)
    {
        if (empty($ids)) return;
        //$book->authors()->attach($request->authors); // при условии, что authors это уже массив с id, иначе id нужно сначала получить (Arr::pluck(), например)
        $this->exclude_works()->attach($ids);
    }

    public  function add2favorite($id)
    {
        if (empty($id)) return;

        $this->favorite_works()->attach($id);
    }

    public function getExcludeWorkds()
    {
        $ids_exclude_workds = $this->exclude_works->pluck('id')->toArray();
        $ids_exclude_workds = array_merge($ids_exclude_workds, $this->favorite_works->pluck('id')->toArray());

        return $ids_exclude_workds;
    }


}
