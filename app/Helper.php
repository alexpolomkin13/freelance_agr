<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Helper extends Model
{
    public static function checkIssetNumbers($str)
    {
        preg_match('/(\d+)/s', $str, $res);
        if (!empty($res[1]))
            return true;
        return false;
    }

    public static function isDollar($str)
    {
        $pos = stripos($str, '$');
        if (isset($pos))
            return true;
        return false;
    }

    public static function getNumberPart($str)
    {
        preg_match('/(\d+)/s', $str, $res);
        if (!empty($res[1]))
            return $res[1];
        return false;
    }
}
