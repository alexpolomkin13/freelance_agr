<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Freelance extends Model
{
    use Sluggable;

    protected $fillable = ['name', 'url'];

    public static function add($fields)
    {
        $freelance = new static();
        $freelance->fill($fields);
        $freelance->save();

        return $freelance->id;
    }

    public static function checkIsset($name)
    {
        $freelance = Freelance::where('name', '=', $name);
        if ($freelance->exists()) {
            return $freelance->get()->first()->id;
        }

        return false;
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
