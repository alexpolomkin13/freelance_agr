<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\DomCrawler\Crawler;

class Parser extends Model
{
    /**
     * Get content from html.
     *
     * @param $parser array parser settings
     * @param $link string link to html page
     *
     * @param $html
     * @return array with parsing data
     */
    public function getContent(Array $parser, $link, $html)
    {

        // Create new instance for parser.
        $crawler = new Crawler($html, $link);
        //$crawler->addHtmlContent($html, 'UTF-8');
        $crawler = $crawler->filter($parser['settings']['block']);

        $content = $crawler->each(
            function (Crawler $node, $i) use ($parser) {
                $data['title'] = $node->filter($parser["settings"]["title"])->text();
                $data['text'] = $node->filter($parser["settings"]["text"])->text();
                $data['price'] = $node->filter($parser["settings"]["price"])->text();
                $data['detail_link'] = $node->filter($parser["settings"]["detail_link"])->link()->getUri();
                if (!empty($parser["settings"]["category"])) {
                    $data['category'] = $node->filter($parser["settings"]["category"])->text();
                }

                return $data;
            }
        );

        return $content;
    }
}
