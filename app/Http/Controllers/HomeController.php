<?php

namespace App\Http\Controllers;

use App\Note;
use App\User;
use App\Work;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;


class HomeController extends Controller
{
    use ValidatesRequests;

    public function index(Request $request)
    {
        $works = new Work();
        if (Auth::check()) {
            $user = Auth::user();
            $idsAvailableWorkds = $user->getExcludeWorkds();
            $works = $works->whereNotIn('id', $idsAvailableWorkds);
            $notes = Auth::user()->notes()->get();
        } else {
            $notes = null;
        }
      /*  $this->validate($request, [
            'pricefrom' => 'numeric|gt:0',
            'priceto' => 'numeric|gt:0'
        ]);*/
        if ($request->has('keywords') && !empty($request->keywords)) {
            $works = $works->where('title', 'like', "%$request->keywords%");
        }

        if ($request->has('minuswords') && !empty($request->minuswords)) {
            $works = $works->where('title', 'not like', "%$request->minuswords%");
        }

        if ($request->has('price_exist') && $request->price_exist == 1) {
            $works = $works->where('price', '<>', "");
        }

        if ($request->has('categories')) {
            $works = $works->where('category_id', $request->categories);
        }

        if (($request->has('pricefrom') && !empty($request->pricefrom))
            && ($request->has('priceto') && !empty($request->priceto))) {
            $works = $works->where('price', '>', (int)$request->pricefrom)->where('price', '<', (int)$request->priceto);
        } elseif ($request->has('pricefrom') && !empty($request->pricefrom)) {
            $works = $works->where('price', '>', (int)$request->pricefrom);
        } elseif ($request->has('priceto') && !empty($request->priceto)) {
            $works = $works->where('price', '<', (int)$request->priceto);
        }

        $works = $works->orderByDesc('created_at')->paginate(5)->appends([
            'keywords' => request('keywords'),
            'price_exist' => request('price_exist'),
            'categories' => request('categories'),
            'pricefrom' => request('pricefrom'),
            'priceto' => request('priceto'),
        ]);
        return view('front.index', [
            "works" => $works,
            "notes" => $notes
        ]);
    }

    public function exclude_work($id)
    {
        if (Auth::check()) {
            /**
             * После проверки уже можешь получать любое свойство модели
             * пользователя через фасад Auth, например id
             */
            $user = Auth::user();
            $user->excludeWork($id);
            return redirect()->route('home', [
                'keywords' => request('keywords'),
                'price_exist' => request('price_exist'),
                'categories' => request('categories')
            ])->with('status', 'Предложение успешно удалено');
        } else {
            return redirect()->route('home')->with('error', 'Чтобы удалить предложение из списка, пожалуйста, <a href="/login">авторизуйтесь</a>');
        }
    }

    public function favorite_work($id)
    {
        if (Auth::check()) {
            /**
             * После проверки уже можешь получать любое свойство модели
             * пользователя через фасад Auth, например id
             */
            $user = Auth::user();
            $user->add2favorite($id);
            return redirect()->route('home')->with('status', 'Предложение успешно добавлено в избранное');
        } else {
            return redirect()->route('home')->with('error', 'Чтобы добавить предложение в избранное, пожалуйста, <a href="/login">авторизуйтесь</a>');
        }
    }
}
