<?php

namespace App\Http\Controllers;

use App\Category;
use App\Freelance;
use App\Helper;
use App\Logger;
use App\MoneyConvertor;
use App\Parser;
use App\Work;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;
use Goutte\Client;

class Controller extends BaseController
{


    public function test()
    {
        Logger::$PATH = storage_path('logs');
        $logger = Logger::getLogger('freelance');
        $settingsParser = [
            "settings" => [
                "title" => 'div.title > a',
                "text" => 'p.text_field',
                "price" => 'div.title.amount',
                "detail_link" => 'div.title > a',
                "category" => 'div.text-muted.dot_divided > span > a',
                "block" => 'div.cols_table > div.row.click_container-link.set_href'
            ]
        ];
        $idFreelance = Freelance::checkIsset('weblancer');
        if(empty($idFreelance)) {
            $idFreelance = Freelance::add(['name' => 'weblancer', 'url' => 'https://www.weblancer.net/']);
        }
        $categories = Category::all()->pluck('title', 'id')->toArray();
        $works = Work::where('freelance_id', $idFreelance)->pluck('title', 'id')->toArray();
        //инициализация класса конвертора валют

        $converter = new MoneyConvertor();


        $parser = new Parser();
        $content = [];
        for ($i = 1; $i < 2; $i++ )
        {
            $link = "https://www.weblancer.net/jobs/?type=project&page=$i";
            $html = file_get_contents($link);
            $content = $parser->getContent($settingsParser, $link, $html);

            foreach ($content as $item) {
                if (!in_array($item['title'], $works)) {

                    if (Helper::checkIssetNumbers($item['price']) && Helper::isDollar($item['price'])) {
                        $item['price'] = $converter->Ruble2DollarConvertor($item['price']) . 'р.';
                    }

                    if (!in_array($item["category"], $categories)) {
                        $categories[] = $item["category"];
                        $item["category_id"] = Category::add($item["category"]);
                    } else {
                        $item["category_id"] = array_search($item["category"], $categories);
                    }
                    $item["freelance_id"] = $idFreelance;
                    Work::add($item);
                    $logger->log("Работа с url:" . $item['detail_url'] . 'успешно добавлена');
                }
            }

        }
    }
}
