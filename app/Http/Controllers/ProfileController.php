<?php

namespace App\Http\Controllers;

use App\Note;
use App\Work;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    use ValidatesRequests;

    public function index()
    {
        $user = \Auth::user();

        return view('front.profile', [
            'user' => $user
        ]);
    }

    public function update(Request $request)
    {
        $user = \Auth::user();
        $this->validate($request, [
           'name' => 'required',

        ]);
        $user->edit($request->all());

        return redirect()->back()->with('status', 'Профиль успешно обновлён');
    }

    public function showExcludesWorks()
    {
        $user = Auth::user();

        $works = $user->exclude_works()->paginate(5);

        return view('front.excludes_works', [
            'works' => $works
        ]);

    }

    public function showFavoriteWorks()
    {
        $user = Auth::user();

        $works = $user->favorite_works()->paginate(5);

        return view('front.favorite_works', [
            'works' => $works
        ]);

    }

    public function showNotes()
    {
        $user = Auth::user();
        $notes = $user->notes()->get();
        $works_id = [];
        foreach ($notes as $note) {
            $works_id[] = $note->work_id;

        }
        $works_id = array_unique($works_id);
        $works = Work::whereIn('id', $works_id)->orderByDesc('created_at')->get()->keyBy('id');

        return view('front.notes', [
        'works' => $works
    ]);
    }
}
