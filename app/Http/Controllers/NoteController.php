<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    public function create(Request $request, $id)
    {
        if (Auth::check()) {
            $user = Auth::user();
            Note::add([
               'text' => $request->text,
                'user_id' => $user['id'],
                'work_id' => $id,
            ]);
        } else {
            return redirect()->route('home')->with('error', 'Чтобы сделать заметку, пожалуйста, <a href="/login">авторизуйтесь</a>');
        }

        return redirect()->back();
    }
}
