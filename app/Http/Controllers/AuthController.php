<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use ValidatesRequests;

    public function registerForm()
    {
        return view('front.register');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:users',
            'password' => 'required'
        ]);
        $user = User::add($request->all());

        return redirect('/login');
    }

    public function loginForm()
    {
        return view('front.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required'
        ]);
        $credentials = $request->only('name', 'password');
        if (\Auth::attempt($credentials)) {
            return redirect('/')->with('status', 'Вы успешно авторизованы!');
        } else {
            return redirect()->back()->with('status', 'Неправильный логин или пароль');
        }
    }

    public function logout()
    {
        \Auth::logout();

        return redirect('/');
    }
}
